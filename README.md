### Running the application

###### Using the pipeline artifact

- Download the deployment artifact and inside the dist folder run the `node index.js ` command.

###### Downloading the repository

- Inside the root of the project, run an npm install command
- Run the `build` command
- You can both use `node index.js` command inside the dist folder or `npm start` if you want to debug the application.
